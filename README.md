#### To run the project:
`docker-compose up`

#### To send data:
`watch -n 0.1 curl -H "Content-Type: application/json" -X POST -d '{"foo":"bar"}' http://{YOUR_IP}:8082/contentListener`
